<?php

namespace Kgurovoy\Tor\Facades;

use Illuminate\Support\Facades\Facade;

class ChangerIP extends Facade 
{
    protected static function getFacadeAccessor() { return 'ChangerIP'; }
}
<?php

return [
    'ip'                => env('TOR_IP', '127.0.0.1'),
    'pass'              => env('TOR_PASS', ''),
    'control_port'      => env('TOR_CONTROL_PORT', '9051'),
    'connection_port'   => env('TOR_CONNECTION_PORT', '9050'),
];

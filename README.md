## About

Changer TOR IP

## Install

- "kgurovoy/changer-ip": "^1.0"
- "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:kgurovoy/changer-ip.git"
        }
    ],
- php composer.phar update
- php artisan vendor:publish

## Use

- Dynamic: (new ChangerIP())->change();
- Facade: ChangerIP::change();
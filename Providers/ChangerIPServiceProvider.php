<?php

namespace Kgurovoy\Tor\Providers;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class ChangerIPServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/changer-ip.php' => config_path('changer-ip.php'),
        ]);

        App::bind('ChangerIP', function()
        {
            return new \Kgurovoy\Tor\ChangerIP;
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this
            ->app
            ->singleton('changer-ip.ip', function () {
                $config = Config::get('changer-ip.ip');

                return ClientBuilder::fromConfig($config);
            });
    }
}

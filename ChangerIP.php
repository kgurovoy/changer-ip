<?php

namespace Kgurovoy\Tor;

class ChangerIP
{
    /**
     * ChangerIP constructor.
	 *
     * @throws \Exception
     */
	public function __construct(int $controlPort = null, int $connectionPort = null)
	{
		$this->set($controlPort, $connectionPort);
	}

    /**
     * Change ip TOR
	 *
     * @return string
     */
    public function changeIp(int $controlPort = null)
    {
        if (!empty($controlPort)) $this->set($controlPort);

	    $fp = fsockopen($this->torIp, $this->torControlPort, $errno, $errstr, 30);

		if (!$fp) throw new \Exception('Cant connect to TOR port');

		fputs($fp, "AUTHENTICATE \"". config('changer-ip.pass') ."\"\r\n");
		$response = fread($fp, 1024);

		list($code, $text) = explode(' ', $response, 2);

		if ($code !== '250') throw new \Exception('Authentication failed');

		fputs($fp, "SIGNAL NEWNYM\r\n");
		$response = fread($fp, 1024);
		list($code, $text) = explode(' ', $response, 2);

		fclose($fp);
		if ($code !== '250') {
		    throw new \Exception('Error');
		}

		$this->changed = true;

		return $this;
    }

    /**
     * Get IP
     *
     * @param null $url
     * @param bool $json
     * @param null $key
     * @return ChangerIP
     */
    public function getIp(int $connectionPort = null, $url = null, $jsonKey = false): ChangerIP
    {
        if (empty($url)) $url = 'https://ipinfo.tw/ip';

        if (!empty($connectionPort)) $this->set(null, $connectionPort);

        $response = $this->checkIp($url);

        if ($jsonKey) $response = $this->parseJson($response, $jsonKey);

        $this->ip = trim($response);

        return $this;
    }

    /**
     * Check IP
     *
     * @param $url
     * @return string
     */
    private function checkIp($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0 );
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_PROXY, "$this->torIp:$this->torConnectionPort");
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        if ($header_size === 0) throw new \Exception('Problems with internet connection or proxy');

        curl_close($ch);

        return $response;
    }

    /**
     * Parse response
     *
     * @param $response
     * @param $jsonKey
     * @return mixed
     */
    private function parseJson($response, $jsonKey)
    {
        try {
            return json_decode($response)->$jsonKey;
        } catch (\Exception $exception) {
            return $exception;
        }
    }

    /**
     * @param int|null $port
     * @param int|null $connectionPort
     * @throws \Exception
     */
    private function set(int $controlPort = null, int $connectionPort = null)
    {
        if (empty(config('changer-ip')))    throw new \Exception('Settings not found');
        if (empty(config('changer-ip.ip'))) throw new \Exception('Tor ip not found');
        $this->torControlPort       = !empty($controlPort) ? $controlPort : config('changer-ip.control_port');
        $this->torConnectionPort    = !empty($connectionPort) ? $connectionPort : config('changer-ip.connection_port');
        $this->torIp                = config('changer-ip.ip');
    }
}
